# Fundamentos de Microfrontends: Ejercicio Práctico

## Microfrontend Comentarios ##

### Enunciado ###

Desarrollar un listado de comentarios, este debe recibir un evento disparado en el microfrontend de sugerencias para listar los comentarios del video que el usuario selecciono.
[Prototipo Figma: Ejercicio Práctico 2](https://www.figma.com/design/x9uqJOn72FkfWW0xMFqBvk/Pruebas?node-id=66-47&t=ROcJPDHLFIyKWPZU-0)

### Objetivos ###
- Comprender los fundamentos de microfrotends y como se comunican entre sí.
- Desarrollo orientado a componentes basado en figma.
- Análisis e implementación de patrón vuex.
- Análisis e implementación de clean architecture.

### Consideraciones ###
- No es necesario implementar un backend para obtener los datos, puede usar data dummy o de internet.
- Tener instalado NodeJs v16.20.2