import { computed } from "vue";
import { useStore } from "vuex";

import {
    COMENTARIOS_STORE_GET_COMENTARIOS,
    COMENTARIOS_STORE_LISTAR_COMENTARIOS,
    COMENTARIOS_STORE_REINICIAR
} from "../../store/business/cmp-comentarios.store";

import { executeAction } from "../../util/vuex.util";

export default function useComentarios() {
    const store = useStore();

    const listarComentarios = (id: number) => {
        executeAction(COMENTARIOS_STORE_LISTAR_COMENTARIOS, id);
    }

    const reiniciar = () => {
        executeAction(COMENTARIOS_STORE_REINICIAR, null);
    }

    const comentarios = computed(
        () => store.getters[COMENTARIOS_STORE_GET_COMENTARIOS]
    );

    return {
        comentarios,
        listarComentarios,
        reiniciar
    }
}