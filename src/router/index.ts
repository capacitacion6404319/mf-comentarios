import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'

import VwComentarios from "../components/business/cmp-comentarios/cmp-comentarios.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",    
    component: VwComentarios
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
