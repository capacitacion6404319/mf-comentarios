import { createStore } from 'vuex'

import ComentariosStore from "./business/cmp-comentarios.store";

export default createStore({
  modules: {
    ComentariosStore
  }
})
