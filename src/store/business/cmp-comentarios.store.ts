export const COMENTARIOS_STORE_GET_COMENTARIOS = "ComentariosStore/getComentarios";

export const COMENTARIOS_STORE_LISTAR_COMENTARIOS = "ComentariosStore/listarComentarios";
export const COMENTARIOS_STORE_REINICIAR = "ComentariosStore/reiniciar";

import { listarSugerencias } from "../../core/comentarios/infraestructure/adapters/in/comentarios.service"

export default {
    namespaced: true,
    state: {
        comentarios: [],
    },
    mutations: {
        LISTAR_COMENTARIOS(state: any, comentarios: []) {
            state.comentarios = comentarios;
        },
        REINICIAR(state: any) {
            state.comentarios = [];
        }
    },
    actions: {
        listarComentarios({ commit }: any, id: number) {
            const comentarios = listarSugerencias(id);
            commit('LISTAR_COMENTARIOS', comentarios);
        },
        reiniciar({ commit }: any) {
            commit('REINICIAR');
        }
    },
    getters: {
        getComentarios(state: any) {
            return state.comentarios;
        }
    }
}