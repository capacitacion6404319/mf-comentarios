import { defineComponent } from "vue";

import CmpComentarios from "../../components/business/cmp-comentarios/cmp-comentarios.vue";

export default defineComponent({
    components: {
        CmpComentarios
    }
})