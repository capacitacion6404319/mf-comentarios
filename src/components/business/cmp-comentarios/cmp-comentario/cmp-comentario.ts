import { defineComponent } from "vue";

import { PATH_RESOURCES } from "../../../../constants/app.constant";

export default defineComponent({
    props: {
        item: {
            type: Object,
            default: null
        }
    },
    setup() {
        const PREFIX = "cmp-comentario";

        return {
            PREFIX,
            PATH_RESOURCES
        }
    }
})
