import { defineComponent, onMounted, onUnmounted } from "vue";

import CmpComentario from "./cmp-comentario/cmp-comentario.vue";

import useComentarios from "../../../composables/business/cmp-comentarios.composable";

export default defineComponent({
    components: {
        CmpComentario
    },
    setup() {
        const PREFIX = "cmp-comentarios";

        const { comentarios, listarComentarios, reiniciar} = useComentarios();

        const handleEventCmpSugerenciaSeleccionarVideo = (event: any) => {
            const detail = event["detail"];
            if (detail) {
                const item = detail["item"];
                listarComentarios(item.id);
            }
        };

        onMounted(() => {
            window.addEventListener('evt-cmp-sugerencia-seleccionar-video', handleEventCmpSugerenciaSeleccionarVideo);
        })

        onUnmounted(() => {
            window.removeEventListener('evt-cmp-sugerencia-seleccionar-video', handleEventCmpSugerenciaSeleccionarVideo);
        })

        onUnmounted(() => {
            reiniciar();
        })

        return {
            PREFIX,
            comentarios
        }
    }
})
