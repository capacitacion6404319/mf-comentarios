import { SingletonBase } from "../../../shared/domain/singleton-base.domain";
import { ListarComentariosPort } from "../../infraestructure/ports/in/listar-comentarios.port";
import { Comentario } from "../../domain/comentario.domain";

import comentarios from "../../../../assets/resources/data-dummy/comentarios.json";

export class ListarComentariosUseCase extends SingletonBase implements ListarComentariosPort {

    constructor() {
        super();
    }

    listarComentarios(id: number): Comentario[] {
        const item = comentarios.find(x => x.id === id);
        return item ? item.comments : [];
    }
}