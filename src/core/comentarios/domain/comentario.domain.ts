export class Comentario {
    id?: number;
    photo?: string;
    email?: string;
    date?: string;
    comment?: string;
    likes?: string;
}