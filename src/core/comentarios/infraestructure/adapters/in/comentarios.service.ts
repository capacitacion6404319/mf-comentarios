import { ListarComentariosPort } from "../../ports/in/listar-comentarios.port";

import { ListarComentariosUseCase } from "../../../application/use-case/listar-comentarios.use-case";

export const listarSugerencias = (id: number) => {
    const listarComentariosPort: ListarComentariosPort = ListarComentariosUseCase.getInstance(ListarComentariosUseCase);
    return listarComentariosPort.listarComentarios(id);
}