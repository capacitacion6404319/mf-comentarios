import { Comentario } from "../../../domain/comentario.domain";

export interface ListarComentariosPort {
    listarComentarios(id: number): Comentario[];
}