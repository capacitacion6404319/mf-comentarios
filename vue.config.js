const path = require('path');
const fs = require('fs');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const EventHooksPlugin = require('event-hooks-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
  chainWebpack(config) {
    config.plugin('SystemJSPublicPathWebpackPlugin').tap((args) => {
      args[0].rootDirectoryLevel = 1;
      return args;
    });
    config.devServer.headers({
      'Access-Control-Allow-Origin': '*',
    });
    config.devServer.set('port', 8082);
    config.output.filename('[name].js');
    config.output.publicPath('/');
    config.plugin('copy-webpack-plugin').use(CopyWebpackPlugin, [
      {
        patterns: [
          {
            from: 'src/assets/resources', // Ruta de tus imágenes fuente
            to: 'resources', // Ruta de destino en la carpeta de salida (dist/)
          }
        ],
      },
    ]);    
  },
  lintOnSave: true,
  filenameHashing: false,
  configureWebpack: {
    optimization: {
      minimize: true,
      minimizer: [
        new CssMinimizerPlugin(),
        new TerserPlugin({
          terserOptions: {
            ecma: 2015, // Otra opción es utilizar ecma: 2018 según tus necesidades
            mangle: true,
            output: {
              comments: false,
            },
            compress: {
              drop_console: true, // Elimina console.log y similares
              unused: true, // Elimina código no utilizado
            },
          },
          include: [
            /vue-i18n\.mjs/,
            /message-compiler\.esm-browser\.js/,
            /eventemitter2\.js/,
            /runtime-core\.esm-bundler\.js/,
            /index-browser-esm\.js/
          ]
        })
      ],
    },
    module: {
      rules: [
        {
          test: /\.(png|jpg|jpeg|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[hash].[ext]',
                outputPath: 'resources/'
              },
            },
          ],
        },
        {
          test: /\.m?js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: '> 0.25%, not dead',
                    modules: false,
                  },
                ],
              ],
            },
          }
        }
      ]
    },    
    plugins: [
      new EventHooksPlugin({
        done: () => {
          if (process.env.NODE_ENV !== 'development') {
            const buildDir = path.join(__dirname, '/dist');
            fs.unlinkSync(`${buildDir}/index.html`);
          }
        },
      }),
    ],
    externals: ['single-spa-vue',/^@sreasons\/.+/],
    output: {
      libraryTarget: 'system',
    },
  },
}